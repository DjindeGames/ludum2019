﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableLevel : ScriptableObject
{
    public float duration;
    public float spawnCooldown;
    public float[] spawnProbabilities;
}
