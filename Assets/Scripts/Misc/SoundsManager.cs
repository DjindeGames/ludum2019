﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundType { Fwish, Poc, Kling, Eow, Heho }

public class SoundsManager : MonoBehaviour
{
    public AudioClip[] fwishes;
    public AudioClip[] pocs;
    public AudioClip[] klings;
    public AudioClip[] eows;
    public AudioClip[] hehos;
    public GameObject volatileSource;

    public static SoundsManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public void PlaySound(SoundType type, Vector3 position)
    {
        if (ZeroController.Instance != null && Vector3.Distance(position, ZeroController.Instance.transform.position) <= 10)
        {
            GameObject instance = Instantiate(volatileSource, position, Quaternion.identity, null);
            AudioSource attachedSource = instance.GetComponent<AudioSource>();
            AudioClip sound = null;
            switch (type)
            {
                case (SoundType.Fwish):
                    sound = fwishes[Random.Range(0, fwishes.Length)];
                    break;
                case (SoundType.Kling):
                    sound = klings[Random.Range(0, klings.Length)];
                    break;
                case (SoundType.Poc):
                    sound = pocs[Random.Range(0, pocs.Length)];
                    break;
                case (SoundType.Eow):
                    sound = eows[Random.Range(0, eows.Length)];
                    break;
                case (SoundType.Heho):
                    sound = hehos[Random.Range(0, hehos.Length)];
                    break;
            }
            attachedSource.clip = sound;
            attachedSource.Play();
            Destroy(instance, sound.length);
        }
    }
}
