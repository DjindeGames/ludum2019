﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyBoardType { Qwerty, Azerty }

public class KeyboardSettings : MonoBehaviour
{
    public static KeyboardSettings Instance { get; private set; }
    public KeyBoardType Type { get; set; } = KeyBoardType.Qwerty;
    public KeyCode Up { get => (Type == KeyBoardType.Qwerty) ? KeyCode.W : KeyCode.Z; }
    public KeyCode Down { get => KeyCode.S; }
    public KeyCode Left { get => (Type == KeyBoardType.Qwerty) ? KeyCode.A : KeyCode.Q; }
    public KeyCode Right { get => KeyCode.D; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

    public KeyBoardType ChangeKeyBoardType()
    {
        if (Type == KeyBoardType.Qwerty)
        {
            Type = KeyBoardType.Azerty;
        } else
        {
            Type = KeyBoardType.Qwerty;
        }
        return Type;
    }
}
