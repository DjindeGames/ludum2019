﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public TMP_Text KeyBoardText;
    public GameObject rules;
    public AudioClip[] specialSounds;
    public AudioSource soundSource;

    private void Start()
    {
        rules.SetActive(false);
        KeyBoardText.text = "KEYBOARD: " + KeyboardSettings.Instance.Type.ToString().ToUpper();
    }

    public void StartArena()
    {
        SceneManager.LoadScene("Arena");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void ChangeKeyBoardSettings()
    {
        KeyBoardType newType = KeyboardSettings.Instance.ChangeKeyBoardType();
        KeyBoardText.text = "KEYBOARD: " + newType.ToString().ToUpper();
    }

    public void PlaySpecialSound()
    {
        soundSource.clip = specialSounds[Random.Range(0, specialSounds.Length)];
        soundSource.Play();
    }

    public void DisplayRules()
    {
        rules.SetActive(true);
    }

    public void HideRules()
    {
        rules.SetActive(false);
    }
}
