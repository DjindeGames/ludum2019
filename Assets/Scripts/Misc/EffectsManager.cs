﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectType { Spawn, Multiplied, Divided, Collision }

public class EffectsManager : MonoBehaviour
{
    public GameObject spawnPrefab;
    public GameObject wallCollisionPrefab;
    public GameObject multipliedPrefab;
    public GameObject dividedPrefab;

    public static EffectsManager Instance { get; private set; }

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    public void SpawnEffect(EffectType type, Vector3 position)
    {
        GameObject instance = null;
        switch (type)
        {
            case (EffectType.Collision):
                instance = Instantiate(wallCollisionPrefab, position, wallCollisionPrefab.transform.rotation, null);
                break;
            case (EffectType.Multiplied):
                instance = Instantiate(multipliedPrefab, position, multipliedPrefab.transform.rotation, null);
                break;
            case (EffectType.Divided):
                instance = Instantiate(dividedPrefab, position, dividedPrefab.transform.rotation, null);
                break;
            case (EffectType.Spawn):
                instance = Instantiate(spawnPrefab, position, spawnPrefab.transform.rotation, null);
                break;
        }
        Destroy(instance, 1f);
    }
}
