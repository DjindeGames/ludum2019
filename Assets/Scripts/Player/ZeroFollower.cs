﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZeroFollower : MonoBehaviour
{
    public float minDistanceToTarget;
    public Transform target;
    private NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, target.position) > minDistanceToTarget)
        {
            agent.isStopped = false;
            agent.destination = target.position;
        } else
        {
            agent.isStopped = true;
        }
    }
}
