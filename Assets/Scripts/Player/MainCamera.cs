﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public Transform target;
    public float offSet;

    public static MainCamera Instance { get; private set; }

    private void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = new Vector3(mousePos.x, transform.position.y, mousePos.z) - new Vector3(target.position.x, transform.position.y, target.position.z);
        transform.position = new Vector3(target.position.x, transform.position.y, target.position.z) + Vector3.ClampMagnitude(direction, offSet);
    }
}
