﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroController : MonoBehaviour
{
    public GameObject multiplyPrefab;
    public GameObject dividePrefab;
    private CharacterController controller;
    private GameManagement manager;

    public static ZeroController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        manager = GameManagement.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        float speed = manager.CurrentPlayerSpeed;
        float xDir = 0;
        float yDir = 0;

        if (Input.GetKey(KeyboardSettings.Instance.Up))
        {
            yDir += speed;
        }
        if (Input.GetKey(KeyboardSettings.Instance.Down))
        {
            yDir -= speed;
        }
        if (Input.GetKey(KeyboardSettings.Instance.Left))
        {
            xDir -= speed;
        }
        if (Input.GetKey(KeyboardSettings.Instance.Right))
        {
            xDir += speed;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Fire(MathProjectileType.Divide);
        }
        if (Input.GetMouseButtonDown(0))
        {
            Fire(MathProjectileType.Divide);
        } else if (Input.GetMouseButtonDown(1) && GameManagement.Instance.MultiplyAvailable)
        {
            StartCoroutine(GameManagement.Instance.StartMultiplyCooldown());
            Fire(MathProjectileType.Multiply);
        }
        Vector3 cameraPos = MainCamera.Instance.transform.position;
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //transform.rotation = Quaternion.LookRotation((new Vector3(mousePos.x, 0, mousePos.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized);
        Move(new Vector3(xDir, 0, yDir));
    }

    private void Fire(MathProjectileType type)
    {
        SoundsManager.Instance.PlaySound(SoundType.Fwish, transform.position);
        GameObject projectileInstance = null;
        if (type == MathProjectileType.Multiply)
        {
            projectileInstance = Instantiate(multiplyPrefab, transform.position, multiplyPrefab.transform.rotation, null);
        } else
        {
            projectileInstance = Instantiate(dividePrefab, transform.position, multiplyPrefab.transform.rotation, null);
        }
        MathProjectile projectile = projectileInstance.GetComponent<MathProjectile>();
        Vector3 cameraPos = MainCamera.Instance.transform.position;
        projectile.direction = (new Vector3(cameraPos.x, 0, cameraPos.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;
    }

    private void Move(Vector3 direction)
    {
        controller.Move(direction * Time.deltaTime);
    }
}
