﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagement : MonoBehaviour
{
    public float playerSpeed;
    public float speedMalus = 0.05f;
    private float speedMultiplier = 1;
    public GameObject scoreObject;
    public TMP_Text scoreText;
    public TMP_Text finalScoreText;
    public TMP_Text finalLevelText;
    public TMP_Text healthText;
    public TMP_Text levelText;
    public GameObject zeroPrefab;
    public GameObject onePrefab;
    public Transform zeroFollowers;
    public Image multiplyLogo;
    public Slider multiplyCooldownSlider;
    public int multiplyCooldown;
    public GameObject gameOverScreen;
    public bool immortal = false;
    public GameObject pauseMenu;
    public ScriptableLevel[] levels;
    public int currentLevel = 0;

    private int score;
    private bool cooldown = false;
    public int playerHealth = 1;

    public float CurrentPlayerSpeed { get => playerSpeed * speedMultiplier; }
    public static GameManagement Instance { get; private set; }
    public bool MultiplyAvailable { get; private set; } = true;
    bool init = false;

    private void Awake()
    {
        Instance = this;
        multiplyCooldownSlider.maxValue = multiplyCooldown;
        multiplyCooldownSlider.value = multiplyCooldown;
        gameOverScreen.SetActive(false);
        pauseMenu.SetActive(false);
    }
    private IEnumerator Start()
    {
        EnnemySpawner.Instance.LevelMaxedDuration = levels[levels.Length - 1].spawnCooldown;
        levelText.text = currentLevel.ToString();
        yield return new WaitForSeconds(3);
        StartCoroutine(PlayLevel());
    }

    private IEnumerator PlayLevel()
    {
        ScriptableLevel level;
        levelText.text = currentLevel.ToString();
        finalLevelText.text = currentLevel.ToString();
        if (currentLevel < levels.Length)
        {
            level = levels[currentLevel];
            EnnemySpawner.Instance.CurrentLevel = level;
        } else
        {
            level = levels[levels.Length - 1];
            EnnemySpawner.Instance.LevelMaxedDuration *= 0.95f;
            EnnemySpawner.Instance.LevelMaxed = true;
        }
        if (!init)
        {
            init = true;
            EnnemySpawner.Instance.Begin();
        }
        yield return new WaitForSeconds(level.duration);
        currentLevel++;
        StartCoroutine(PlayLevel());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)){
            if (pauseMenu.activeSelf)
            {
                Resume();
            } else if (!gameOverScreen.activeSelf)
            {
                Pause();
            }
        }
    }

    public void IncreaseScore(int scoreAdded)
    {
        score += scoreAdded;
        scoreText.text = score.ToString();
        finalScoreText.text = score.ToString();
    }

    public void TakeDamage(bool plussed)
    {
        if (!cooldown && !immortal)
        {
            StartCoroutine(DamageCooldown());
            playerHealth--;
            healthText.text = playerHealth.ToString();
            SoundsManager.Instance.PlaySound(SoundType.Eow, transform.position);
            //Debug.Log("Damage Taken, " + playerHealth + " HP remaining !");
            if (playerHealth <= 0)
            {
                StartCoroutine(GameOver());
            }
            else if (playerHealth >= 1)
            {
                if (playerHealth >= 4)
                {
                    speedMultiplier += speedMalus;
                }
                EffectsManager.Instance.SpawnEffect(EffectType.Spawn, ZeroController.Instance.transform.position);
                GameObject zeroDestroyed = zeroFollowers.GetChild(zeroFollowers.childCount - 1).gameObject;
                if (plussed)
                {
                    Instantiate(onePrefab, zeroDestroyed.transform.position, onePrefab.transform.rotation, null);
                }
                EffectsManager.Instance.SpawnEffect(EffectType.Multiplied, zeroDestroyed.transform.position);
                Destroy(zeroDestroyed);
            }
        }
    }

    private IEnumerator GameOver()
    {
        EnnemySpawner.Instance.Stopped = true;
        EffectsManager.Instance.SpawnEffect(EffectType.Divided, ZeroController.Instance.transform.position);
        MainCamera.Instance.enabled = false;
        ZeroController.Instance.enabled = false;
        Destroy(ZeroController.Instance.gameObject);
        yield return new WaitForSeconds(2);
        scoreObject.SetActive(false);
        Time.timeScale = 0;
        gameOverScreen.SetActive(true);
    }

    public void Retry()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    internal void SpawnZero(Vector3 position)
    {
        playerHealth++;
        if (speedMultiplier > 0.1f && playerHealth > 4)
        {
            speedMultiplier -= speedMalus;
        }
        healthText.text = playerHealth.ToString();
        //Debug.Log("+1 HP, " + playerHealth + " remaining.");
        Transform target;
        if (zeroFollowers.childCount > 0)
        {
            target = zeroFollowers.GetChild(zeroFollowers.childCount - 1);
        } else
        {
            target = ZeroController.Instance.transform;
        }
        GameObject zero = Instantiate(zeroPrefab, position, zeroPrefab.transform.rotation, zeroFollowers);
        zero.GetComponent<ZeroFollower>().target = target;
    }

    public IEnumerator StartMultiplyCooldown()
    {
        MultiplyAvailable = false;
        multiplyLogo.enabled = false;
        float cooldownValue = 0;
        while (cooldownValue < multiplyCooldown)
        {
            multiplyCooldownSlider.value = cooldownValue;
            yield return new WaitForEndOfFrame();
            cooldownValue += Time.deltaTime;
        }
        multiplyCooldownSlider.value = cooldownValue;
        multiplyLogo.enabled = true;
        MultiplyAvailable = true;
    }

    public void UpdateEnnemy(bool plussed, Ennemy updated)
    {
        int ennemyIndex = updated.scoreValue - 1;
        SoundsManager.Instance.PlaySound(SoundType.Poc, updated.transform.position);
        if (plussed)
        {
            EffectsManager.Instance.SpawnEffect(EffectType.Spawn, updated.transform.position);
            if (ennemyIndex < 8)
            {
                EnnemySpawner.Instance.UpdateSpawn(ennemyIndex + 1, updated.transform.position);
                updated.Kill(false, false);
            } 
        } else
        {
            if (ennemyIndex > 0)
            {
                EnnemySpawner.Instance.UpdateSpawn(ennemyIndex - 1, updated.transform.position);
                EffectsManager.Instance.SpawnEffect(EffectType.Spawn, updated.transform.position);
            } else
            {
                EffectsManager.Instance.SpawnEffect(EffectType.Divided, updated.transform.position);
            }
            updated.Kill(false, false);
        }
    }

    private IEnumerator DamageCooldown()
    {
        cooldown = true;
        yield return new WaitForSeconds(0.1f);
        cooldown = false;
    }

    public void Exit()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void Pause()
    {
        MainCamera.Instance.enabled = false;
        ZeroController.Instance.enabled = false;
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        MainCamera.Instance.enabled = true;
        ZeroController.Instance.enabled = true;
        pauseMenu.SetActive(false);
    }
}
