﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Six : Ennemy
{
    public GameObject[] ennemiesPrefabs; 

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void PerformAttack()
    {
        // Spawning
        GameObject ennemyPrefab = ennemiesPrefabs[Random.Range(0, ennemiesPrefabs.Length)];
        Instantiate(ennemyPrefab, transform.position, ennemyPrefab.transform.rotation, null);
        // Son et SFX
        EffectsManager.Instance.SpawnEffect(EffectType.Spawn, transform.position);
        SoundsManager.Instance.PlaySound(SoundType.Poc, transform.position);
    }
}
