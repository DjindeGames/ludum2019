﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemySpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] ennemiesPrefabs;

    public static EnnemySpawner Instance { get; private set; }
    public bool Stopped { get; set; } = false;
    public ScriptableLevel CurrentLevel { get; set; }
    public float LevelMaxedDuration { get; set; }
    public bool LevelMaxed { get; set; } = false;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        UnityEngine.Random.InitState(DateTime.Now.Millisecond);
    }

    private IEnumerator SpawnEnnemies()
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            Transform spawnPoint = spawnPoints[i];
            GameObject ennemyPrefab = PickEnnemy();
            //GameObject ennemyPrefab = ennemiesPrefabs[7];
            if (ennemyPrefab != null)
            {
                Instantiate(ennemyPrefab, spawnPoint.position, ennemyPrefab.transform.rotation, null);
                EffectsManager.Instance.SpawnEffect(EffectType.Spawn, spawnPoint.position);
                SoundsManager.Instance.PlaySound(SoundType.Poc, spawnPoint.position);
            }
        }
        if (!Stopped)
        {
            float cooldown = (LevelMaxed) ? LevelMaxedDuration : CurrentLevel.spawnCooldown;
            yield return new WaitForSeconds(cooldown);
            StartCoroutine(SpawnEnnemies());
        }
        /*
        Transform spawnPoint = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)];
        GameObject ennemyPrefab = ennemiesPrefabs[UnityEngine.Random.Range(0, ennemiesPrefabs.Length)];
        //GameObject ennemyPrefab = ennemiesPrefabs[7];
        Instantiate(ennemyPrefab, spawnPoint.position, ennemyPrefab.transform.rotation, null);
        EffectsManager.Instance.SpawnEffect(EffectType.Spawn, spawnPoint.position);
        SoundsManager.Instance.PlaySound(SoundType.Poc, spawnPoint.position);
        if (!Stopped)
        {
            yield return new WaitForSeconds(CurrentLevel.spawnCooldown);
            StartCoroutine(SpawnEnnemy());
        }
        */
    }

    internal void Begin()
    {
        StartCoroutine(SpawnEnnemies());
    }

    public Vector3 GetFarthestSpawn(Vector3 from)
    {
        Vector3 farthestSpawn = spawnPoints[0].position;
        float maxDistance = 0;
        for (int i = 0; i <spawnPoints.Length; i++)
        {
            Vector3 spawnPos = spawnPoints[i].position;
            float distance = Vector3.Distance(from, spawnPos);
            if (distance > maxDistance)
            {
                farthestSpawn = spawnPos;
                maxDistance = distance;
            }
        }
        return farthestSpawn;
    }

    public void UpdateSpawn(int which, Vector3 pos)
    {
        GameObject ennemyPrefab = ennemiesPrefabs[which];
        Instantiate(ennemyPrefab, pos, ennemyPrefab.transform.rotation, null);
        EffectsManager.Instance.SpawnEffect(EffectType.Spawn, pos);
        SoundsManager.Instance.PlaySound(SoundType.Poc, pos);
    }

    private GameObject PickEnnemy()
    {
        GameObject ennemy = null;
        float score = UnityEngine.Random.Range(0,101);
        float browsedProbas = 0;
        int i = 0;
        for(i = 0; i < CurrentLevel.spawnProbabilities.Length; i++)
        {
            browsedProbas += CurrentLevel.spawnProbabilities[i];
            if (score <= browsedProbas)
            {
                break;
            }
        }
        if (i > 0)
        {
            try
            {
                ennemy = ennemiesPrefabs[i - 1];
            } catch (IndexOutOfRangeException)
            {
                Debug.Log("Score = " + score + ", i = " + i);
            }
        }
        return ennemy;
    }
}
