﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum BehaviourType { Bold, Coward, Sniper }

public class Ennemy : MonoBehaviour
{
    public int scoreValue;
    public float minDistanceToPlayer;
    public float coolDown;
    public float fireRange;
    public float projectileSpeed;
    public BehaviourType behaviourType;
    private NavMeshAgent agent;
    protected Transform player;
    private bool fleeing = false;
    

    // Start is called before the first frame update
    protected virtual void Start()
    {
        if (ZeroController.Instance != null)
        {
            player = ZeroController.Instance.transform;
        }
        agent = GetComponent<NavMeshAgent>();
        StartCoroutine(Attack());
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        try
        {
            if (player != null && transform != null)
            {
                Physics.Raycast(transform.position, (player.position - transform.position).normalized, out RaycastHit hit, float.MaxValue, LayerMask.GetMask("Sight"));
                if (behaviourType == BehaviourType.Coward)
                {
                    if (!fleeing)
                    {
                        if (!hit.collider.CompareTag("Wall"))
                        {
                            agent.isStopped = false;
                            agent.destination = EnnemySpawner.Instance.GetFarthestSpawn(transform.position);
                            fleeing = true;
                        }
                        else
                        {
                            transform.LookAt(player);
                            agent.isStopped = true;
                        }
                    }
                    else
                    {
                        if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0)
                        {
                            fleeing = false;
                        }
                    }

                }
                else if (behaviourType == BehaviourType.Bold)
                {
                    if (Vector3.Distance(transform.position, player.position) >= minDistanceToPlayer || hit.collider.CompareTag("Wall"))
                    {
                        agent.isStopped = false;
                        agent.destination = player.position;
                    }
                    else
                    {
                        agent.isStopped = true;
                        transform.LookAt(player);
                    }
                }
                else
                {
                    if (!fleeing)
                    {
                        if (Vector3.Distance(transform.position, player.position) < minDistanceToPlayer && !hit.collider.CompareTag("Wall"))
                        {
                            agent.isStopped = false;
                            fleeing = true;
                            agent.destination = EnnemySpawner.Instance.GetFarthestSpawn(transform.position);
                        }
                        else if (hit.collider.CompareTag("Wall"))
                        {
                            agent.isStopped = false;
                            agent.destination = player.position;
                        }
                        else
                        {
                            agent.isStopped = true;
                            transform.LookAt(player);
                        }
                    }
                    else
                    {
                        if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0)
                        {
                            fleeing = false;
                        }
                    }
                }
            }
        }
        catch (NullReferenceException)
        {

        }
    }

    protected virtual IEnumerator Attack()
    {
        yield return new WaitForSeconds(coolDown);
        if (player != null)
        {
            if (Vector3.Distance(transform.position, player.position) <= fireRange)
            {
                PerformAttack();
            }
        }
        StartCoroutine(Attack());
    }

    protected virtual void PerformAttack()
    {
    }

    public virtual void Kill(bool multiplied, bool scored)
    {
        if (scored)
        {
            GameManagement.Instance.IncreaseScore(scoreValue);
        }
        if (multiplied)
        {
            GameManagement.Instance.SpawnZero(transform.position);
            EffectsManager.Instance.SpawnEffect(EffectType.Multiplied, transform.position);
        } else
        {
            EffectsManager.Instance.SpawnEffect(EffectType.Divided, transform.position);
        }
        Destroy(gameObject);
    }
}
