﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Two : Ennemy
{
    public GameObject minusPrefab;
    public GameObject plusPrefab;
    private int minusRemainings = 2;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void PerformAttack()
    {
        GameObject projectileInstance;
        if (minusRemainings > 0)
        {
            minusRemainings--;
            projectileInstance = Instantiate(minusPrefab, transform.position, minusPrefab.transform.rotation, null);
        } else 
        {
            minusRemainings = 2;
            projectileInstance = Instantiate(plusPrefab, transform.position, plusPrefab.transform.rotation, null);
        }
        SoundsManager.Instance.PlaySound(SoundType.Fwish, transform.position);
        MathProjectile projectile = projectileInstance.GetComponent<MathProjectile>();
        projectile.speed = projectileSpeed;
        projectile.direction = (new Vector3(player.position.x, 0, player.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;
    }
}
