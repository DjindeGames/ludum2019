﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Five : Ennemy
{
    public GameObject minusPrefab;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void PerformAttack()
    {
        SoundsManager.Instance.PlaySound(SoundType.Fwish, transform.position);

        // Shotgun :
        float angle = 0;
        Vector3 vectorDir = (new Vector3(player.position.x, 0, player.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;
        for (int i = 0; i < 10; i++)
        {
            GameObject projectileInstance = Instantiate(minusPrefab, transform.position, minusPrefab.transform.rotation, null);
            MathProjectile projectile = projectileInstance.GetComponent<MathProjectile>();

            projectile.speed = projectileSpeed;
            projectile.direction = Quaternion.Euler(0,angle,0) * vectorDir;
            angle += 36;
        }

    }
}
