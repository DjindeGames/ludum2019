﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seven : Ennemy
{
    public GameObject minusPrefab;
    public GameObject plusPrefab;
    private int minusRemainings;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void PerformAttack()
    {
        StartCoroutine(SpecialAttack());
    }

    private IEnumerator SpecialAttack()
    {
        if (player != null)
        {
            SoundsManager.Instance.PlaySound(SoundType.Fwish, transform.position);
            GameObject projectileInstance = Instantiate(plusPrefab, transform.position, plusPrefab.transform.rotation, null);
            MathProjectile projectile = projectileInstance.GetComponent<MathProjectile>();
            projectile.speed = projectileSpeed;
            projectile.direction = (new Vector3(player.position.x, 0, player.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < 4; i++)
            {
                SoundsManager.Instance.PlaySound(SoundType.Fwish, transform.position);
                projectileInstance = Instantiate(minusPrefab, transform.position, minusPrefab.transform.rotation, null);
                projectile = projectileInstance.GetComponent<MathProjectile>();
                projectile.speed = projectileSpeed;
                projectile.direction = (new Vector3(player.position.x, 0, player.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
