﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Four : Ennemy
{
    public GameObject plusPrefab;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void PerformAttack()
    {
        SoundsManager.Instance.PlaySound(SoundType.Fwish, transform.position);
        GameObject projectileInstance = Instantiate(plusPrefab, transform.position, plusPrefab.transform.rotation, null);
        MathProjectile projectile = projectileInstance.GetComponent<MathProjectile>();
        projectile.speed = projectileSpeed;
        projectile.direction = (new Vector3(player.position.x, 0, player.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;
    }
}
