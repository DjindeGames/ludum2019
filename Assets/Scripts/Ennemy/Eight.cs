﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eight : Ennemy
{
    public GameObject eightPrefabs;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    protected override void PerformAttack()
    {

        // Spawning
        GameObject ennemyPrefab = eightPrefabs;
        Instantiate(ennemyPrefab, transform.position, ennemyPrefab.transform.rotation, null);
        // Son et SFX
        EffectsManager.Instance.SpawnEffect(EffectType.Spawn, transform.position);
        SoundsManager.Instance.PlaySound(SoundType.Poc, transform.position);
    }
}
