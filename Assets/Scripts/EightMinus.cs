﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EightMinus : MathProjectile
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManagement.Instance.TakeDamage(false);
            SoundsManager.Instance.PlaySound(SoundType.Poc, transform.position);
            SoundsManager.Instance.PlaySound(SoundType.Eow, transform.position);
            if (transform.parent.childCount == 1)
            {
                GetComponentInParent<Ennemy>().Kill(false, false);
            }
            Destroy(gameObject);
        }
    }
}
