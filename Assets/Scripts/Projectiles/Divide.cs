﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Divide : MathProjectile
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") && !other.CompareTag("Projectile"))
        {
            if (other.CompareTag("Ennemy"))
            {
                other.GetComponent<Ennemy>().Kill(false, true);
                SoundsManager.Instance.PlaySound(SoundType.Poc, transform.position);
            } else
            {
                SoundsManager.Instance.PlaySound(SoundType.Kling, transform.position);
                EffectsManager.Instance.SpawnEffect(EffectType.Collision, transform.position);
            }
            Destroy(gameObject);
        }
    }
}
