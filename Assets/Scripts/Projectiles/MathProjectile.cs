﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MathProjectileType { Multiply, Divide }

public class MathProjectile : MonoBehaviour
{
    public float rotationSpeed;
    public float speed;
    public Vector3 direction;
    protected bool startCooldown = true;

    private void Start()
    {
        StartCoroutine(StartCooldown());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, rotationSpeed));
        transform.Translate(direction * speed * Time.deltaTime, Space.World);
    }

    private IEnumerator StartCooldown()
    {
        yield return new WaitForSeconds(0.2f);
        startCooldown = false;
    }
}
