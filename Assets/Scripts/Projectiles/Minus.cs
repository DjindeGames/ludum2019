﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minus : MathProjectile
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Projectile") && !other.CompareTag("Ennemy"))
        {
            if (other.CompareTag("Player"))
            {
                GameManagement.Instance.TakeDamage(false);
                SoundsManager.Instance.PlaySound(SoundType.Poc, transform.position);
                SoundsManager.Instance.PlaySound(SoundType.Eow, transform.position);
                Destroy(gameObject);
            }
            else
            {
                SoundsManager.Instance.PlaySound(SoundType.Kling, transform.position);
                EffectsManager.Instance.SpawnEffect(EffectType.Collision, transform.position);
                Destroy(gameObject);
            }
        } 
    }
}
