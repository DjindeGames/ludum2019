﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plus : MathProjectile
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Projectile"))
        {
            if (other.CompareTag("Player"))
            {
                GameManagement.Instance.TakeDamage(true);
                SoundsManager.Instance.PlaySound(SoundType.Poc, transform.position);
                SoundsManager.Instance.PlaySound(SoundType.Eow, transform.position);
                Destroy(gameObject);
            }
            else if (other.CompareTag("Ennemy"))
            {
                if (!startCooldown)
                {
                    GameManagement.Instance.UpdateEnnemy(true, other.GetComponent<Ennemy>());
                    Destroy(gameObject);
                }
            }
            else
            {
                SoundsManager.Instance.PlaySound(SoundType.Kling, transform.position);
                EffectsManager.Instance.SpawnEffect(EffectType.Collision, transform.position);
                Destroy(gameObject);
            }
        }
    }
}
